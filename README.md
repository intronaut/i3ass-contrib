# i3ass-contrib

i3ass-contrib expands the wiki by adding config examples for advanced functionality using budlabs/i3ass. 
It's future aim is to integrate (modified) budlabs/budRich tools -- e.g. i3menu, i3term, i3info, GTFM, linklord, polify, bwp and maybe dunstmerge, gurl, mondo -- into the i3fyra workspace framework for use within i3wm.